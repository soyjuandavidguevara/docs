import Alerta from '/JS/alerta.js';
import Cambiar, {RichText} from '/JS/mostrar.js'
import Slider from '/JS/slider.js';

const location = window.location.href.split('/')

//botones ver mas
if (location[location.length-1]==='mundoinfantil.html' || location[location.length-1]==='entrevoces.html') {
    Cambiar('b1', 'oculto', 'w3-hide');
    Cambiar('b1', 'b1', 'w3-show');
    Cambiar('b1', 'b2', 'w3-hide');
    Cambiar('b2', 'oculto', 'w3-hide');
    Cambiar('b2', 'b1', 'w3-show');
    Cambiar('b2', 'b2', 'w3-hide');
    Cambiar('contenido', 'oculto1', 'w3-hide');
};

if (location[location.length-1]==='entintados.html') {
    Cambiar('b1', 'oculto', 'w3-hide');
    Cambiar('contenido', 'oculto1', 'w3-hide');
} else if (location[location.length-1]!=='index.html' && location[location.length-1]!=='mundoinfantil.html' && 
    location[location.length-1]!=='entrevoces.html' && location[location.length-1]!=='') {
    Cambiar('contenido', 'oculto1', 'w3-hide');
}

//Editor
if (location[location.length-1]==='index.html' || location[location.length-1]==='') {
    Cambiar('mostrarR', 'file', 'w3-hide', ()=>{
        document.getElementById("fe").classList.toggle("w3-hide");
        if(document.getElementById("textr").classList.toggle("w3-hide") == false) {
            document.getElementById("textr").classList.toggle("w3-hide");
        };
    });
    
    Cambiar('mostrarR2', 'textr', 'w3-hide', ()=>{
        if(document.getElementById("file").classList.toggle("w3-hide") === false &&
        document.getElementById("fe").classList.toggle("w3-hide") === false) {
            document.getElementById("file").classList.toggle("w3-hide");
            document.getElementById("fe").classList.toggle("w3-hide");
        };
    });
    
    RichText('#RichText');
};

//SliderCA
if(location[location.length-1]==='codigoarte.html') {
    Slider(".glider");
};

//Alertas

const infoAlerta = {
    icono: 'info',
    titulo: 'Prueba',
    htmlM: '<p>hol<b>a</b></p>',
    ancho: '96%',
    botonCerrar: true,
    infoBoton: 'cerrar',
};

//Alerta(infoAlerta);