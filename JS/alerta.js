
const Alerta = (parametros)=>{
	Swal.fire({
		icon: parametros.icono,
		title: parametros.titulo,
		html: parametros.htmlM,
		footer: parametros.pieHtml,
		width: parametros.ancho,
		showCloseButton: parametros.botonCerrar,
	});	
};

export default Alerta;
